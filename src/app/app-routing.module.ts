import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'login', loadChildren: './pages/login-page/login-page.module#LoginPageModule' },
  { path: 'register', loadChildren: './pages/register-page/register-page.module#RegisterPageModule' },

  { path: '', loadChildren: './layout/layout.module#LayoutModule' },
  { path: '**', loadChildren: './pages/not-found-page/not-found-page.module#NotFoundPageModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
