import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {merge, Observable, of as observableOf} from 'rxjs';
import { Job, JobApiResponse } from '../../model';

@Injectable({
  providedIn: 'root'
})
export class JobService {
  private apiUrl = 'api';
  private apiNode = 'jobs';

  public statuses = {
    complete: { color: 'green', code: 0 },
    incomplete: { color: 'green', code: 1 },
    pending: { color: 'orange', code: 2 },
    rejected: { color: 'red', code: 3 },
  };

  constructor(private http: HttpClient) {}

  public getJobItem(jobId: string) {
    return this.http.get(`${this.apiUrl}/${this.apiNode}`);
  }

  getJobList(sort: string, order: string, page: number, status: string): Observable<any[]> {
    console.log(status);
    const requestUrl = status.length ?
        `${this.apiUrl}/${this.apiNode}?status=${this.statuses[status].code}&_limit=10&_sort=${sort}&_order=${order}&_page=${page + 1}` :
        `${this.apiUrl}/${this.apiNode}?_limit=10&_sort=${sort}&_order=${order}&_page=${page + 1}`;
    return this.http.get<any[]>(requestUrl);
  }
}
