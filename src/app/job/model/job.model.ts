import { Provider } from '@app/provider/model';

export interface Job {
  id: string;
  created_at: string;
  last_modified: string;
  name: string;
  provider: Provider;
  status: string;
  remarks: JobRemark[];
}

export interface JobRemark {
  name: string;
  code: string;
}

export interface JobApiResponse {
  items: Job[];
}
