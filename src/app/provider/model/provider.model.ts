import { User } from '@app/user/model';

export interface Provider {
  name: string;
  staff: User[];
}
