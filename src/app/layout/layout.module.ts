import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import {
  MatListModule,
  MatIconModule,
  MatMenuModule,
  MatButtonModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatToolbarModule,
} from '@angular/material';

import { LayoutModule as SDKLayoutModule } from '@angular/cdk/layout';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout/layout.component';
import { FooterComponent } from './footer/footer.component';
import { JobServiceModule } from '@app/job/service/job';

@NgModule({
  imports: [
    CommonModule,
    MatIconModule,
    MatButtonModule,
    MatListModule,
    MatMenuModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatToolbarModule,
    SDKLayoutModule,
    RouterModule,

    JobServiceModule,
    LayoutRoutingModule
  ],
  declarations: [LayoutComponent, FooterComponent]
})
export class LayoutModule { }
