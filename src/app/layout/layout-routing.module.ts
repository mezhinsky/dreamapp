import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: '', loadChildren: '../pages/dashboard-page/dashboard-page.module#DashboardPageModule' },
      { path: 'jobs', loadChildren: '../pages/jobs-page/jobs-page.module#JobsPageModule' },
      { path: 'users', loadChildren: '../pages/users-page/users-page.module#UsersPageModule' },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
