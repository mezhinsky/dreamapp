import { MediaMatcher } from '@angular/cdk/layout';
import { Component, ChangeDetectorRef, ElementRef, OnInit, NgZone, ViewChild } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { catchError, map, timeout, filter } from 'rxjs/operators';
import { MatSidenav } from '@angular/material/sidenav';
import { JobService } from '@app/job/service/job';



@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  @ViewChild('snav') navigation;

  public winWidth;
  public wrapperWidth = 600;
  public sidenavWidth = 250;
  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;
  fillerNav = Array(50).fill(0).map((_, i) => `Nav Item ${i + 1}`);
  public jobs_show = true;
  public job_statuses;

  constructor(
    private router: Router,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    ngZone: NgZone,
    private jobService: JobService
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);

    this.winWidth = window.innerWidth;
    window.onresize = (e) => {
      ngZone.run(() => {
        this.winWidth = window.innerWidth;
      });
    };
    this.job_statuses = this.jobService.statuses;
  }

  ngOnInit() {
    this.navigation.open();
  }

  // ngOnDestroy(): void {
  //   this.mobileQuery.removeListener(this._mobileQueryListener);
  // }
}
