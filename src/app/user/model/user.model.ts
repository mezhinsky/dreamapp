export interface User {
  username: string;
  firstname: string;
  lastname: string;
  age: string;
  sex: string;
  avatar: string;
}
