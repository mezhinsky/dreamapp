import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JobsPageComponent } from './jobs-page/jobs-page.component';

const routes: Routes = [
  { path: '', component: JobsPageComponent },
  { path: 'incomplete', component: JobsPageComponent },
  { path: 'pending', component: JobsPageComponent },
  { path: 'rejected', component: JobsPageComponent },

  { path: ':status', loadChildren: '../../pages/job-page/job-page.module#JobPageModule' },

  // { path: '**', redirectTo: 'jobs' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JobsPageRoutingModule { }
