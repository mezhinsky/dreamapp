import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort} from '@angular/material';
import {merge, Observable, of as observableOf} from 'rxjs';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import {FormControl} from '@angular/forms';

import {MatTableDataSource} from '@angular/material';


// новый импорт
import {JobService} from '@app/job/service/job';
import {Job} from '@app/job/model';

@Component({
  selector: 'app-jobs-page',
  templateUrl: './jobs-page.component.html',
  styleUrls: ['./jobs-page.component.scss']
})
export class JobsPageComponent implements OnInit {
  displayedColumns: string[] = ['name', 'last_modified', 'provider', 'staff', 'remarks', 'star'];
  dataSource: MatTableDataSource<Job>;
  data: Job[] = [];
  public job_status_filter;

  public statuses = {
    0: 'gray',
    1: 'green',
    2: 'orange',
    3: 'red',
  };

  toppings = new FormControl();
  toppingList: string[] = ['work 1', 'work 2', 'work 3', 'work 4', 'work 5', 'work 6'];

  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private route: ActivatedRoute, private router: Router, private jobService: JobService) {
    this.dataSource = new MatTableDataSource(this.data);
    this.job_status_filter = this.route.snapshot.routeConfig.path;
  }

  ngOnInit() {
    // this.dataSource.sortingDataAccessor = (item, property) => {
    //   switch (property) {
    //     case 'provider': return item.provider.name;
    //     default: return item[property];
    //   }
    // };

    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => {this.paginator.pageIndex = 0; });

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          // tslint:disable-next-line:no-non-null-assertion
          return this.jobService!.getJobList(
            this.sort.active, this.sort.direction, this.paginator.pageIndex, this.job_status_filter);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isRateLimitReached = false;
          // this.resultsLength = data.total_count;
          this.resultsLength = 100;
          return data;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          // Catch if the GitHub API has reached its rate limit. Return empty data.
          this.isRateLimitReached = true;
          return observableOf([]);
        })
      ).subscribe((data: Job[]) => { this.dataSource.data = data; });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  /**
   * deleteItem
   */
  public deleteItem(item) {
    console.log('DETELE SEC TO =>', item);
  }
}
