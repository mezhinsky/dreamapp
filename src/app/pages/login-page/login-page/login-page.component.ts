import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
// import { LoginService } from '../login.service';
// import { slideToTop } from '@app/layout/router.animations';

// import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  public signInForm: FormGroup;

  // public param = {value: 'world'};

  // public langs = [
  //   { title: 'русский', code: 'ru' },
  //   { title: 'english', code: 'en' }
  // ];

  constructor(
    private router: Router,
    // private loginService: LoginService,
    private fb: FormBuilder,
    // public translate: TranslateService
  ) {

    // translate.setDefaultLang('en');
    // translate.use('en');

    this.signInForm = fb.group({
      'username': [null, Validators.compose([Validators.required])],
      'password': [null, Validators.compose([Validators.required])],
    });
  }

  ngOnInit() {
  }

  public submitForm(value: any): void {
    this.router.navigate(['jobs']);
    // this.loginService.signIn(value)
    // .subscribe((response: any) => {
    //   console.log(response);
    //   this.signInForm.reset();
    //   localStorage.setItem('Authorization', response.token);
    // }, error => {
    //   console.log('Ошибка при входе');
    // });
  }

  // public changeLang(code): void {
  //   this.translate.use(code.value);
  // }
}
