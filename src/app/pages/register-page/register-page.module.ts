import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisterPageRoutingModule } from './register-page-routing.module';

@NgModule({
  imports: [
    CommonModule,
    RegisterPageRoutingModule
  ],
  declarations: []
})
export class RegisterPageModule { }
