import { JobPageModule } from './job-page.module';

describe('JobPageModule', () => {
  let jobPageModule: JobPageModule;

  beforeEach(() => {
    jobPageModule = new JobPageModule();
  });

  it('should create an instance', () => {
    expect(jobPageModule).toBeTruthy();
  });
});
