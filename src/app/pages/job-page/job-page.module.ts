import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JobPageRoutingModule } from './job-page-routing.module';
import { JobPageComponent } from './job-page/job-page.component';

@NgModule({
  imports: [
    CommonModule,
    JobPageRoutingModule
  ],
  declarations: [JobPageComponent]
})
export class JobPageModule { }
