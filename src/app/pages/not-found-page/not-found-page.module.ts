import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material';
import { NotFoundPageRoutingModule } from './not-found-page-routing.module';
import { NotFoundPageComponent } from './not-found-page/not-found-page.component';

@NgModule({
  imports: [
    CommonModule,
    MatButtonModule,
    NotFoundPageRoutingModule
  ],
  declarations: [NotFoundPageComponent]
})
export class NotFoundPageModule { }
