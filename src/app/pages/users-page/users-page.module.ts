import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersPageRoutingModule } from './users-page-routing.module';

@NgModule({
  imports: [
    CommonModule,
    UsersPageRoutingModule
  ],
  declarations: []
})
export class UsersPageModule { }
