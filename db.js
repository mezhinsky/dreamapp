const faker = require('faker');


module.exports = () => {

  const statuses = [0,1,2,3];

  const remark_lib = {
    0: [{ name: 'complete', code: 0 }],
    1: [{ name: 'review', code: 1}, { name: 'set provider', code: 2 }, { name: 'forward', code: 3 }],
    2: [{ name: 'evaluate', code: 4}],
    3: [{ name: 'edit', code: 5}],
  }

  Array.prototype.random = function () {
    return this[Math.floor((Math.random() * this.length))];
  }

  let textArray = [
    'male',
    'female',
    'other'
  ];
  var randomNumber = Math.floor(Math.random() * textArray.length);

  function shuffle(array) {
    var i = array.length,
      j = 0,
      temp;

    while (i--) {

      j = Math.floor(Math.random() * (i + 1));

      // swap randomly chosen element with current element
      temp = array[i];
      array[i] = array[j];
      array[j] = temp;

    }

    return array;
  }


  const data = {
    jobs: [],
    users: [],
    providers: []
  }

  //create job
  for (let i = 0; i < 100; i++) {

    let status = statuses.random(),
    selectd_remarks = [];

    for (let statusIndex = 0; statusIndex < Math.floor(Math.random() * (0 - 3 + 1)) + 3; statusIndex++) {

      if(remark_lib[status][statusIndex] != null) {
        selectd_remarks.push(remark_lib[status][statusIndex]);

      }

    }

    let job = {
      id: i,
      created_at: faker.date.past(),
      last_modified: faker.date.past(),
      name: faker.name.jobTitle(),
      provider: null,
      status: status,
      remarks: selectd_remarks
    }

    data.jobs.push(job)
  }

  //create users
  for (let i = 0; i < 1000; i++) {
    let user = {
      id: i,
      username: faker.internet.userName(),
      firstname: faker.name.firstName(),
      lastname: faker.name.lastName(),
      age: Math.floor(Math.random() * (50 - 10 + 1)) + 10,
      sex: textArray[randomNumber],
      avatar: faker.image.avatar()
    }
    data.users.push(user);
  }

  //provider
  let userArrayCount = 0;
  for (let i = 0; i < 100; i++) {
    let provider = {
      id: i,
      name: faker.company.companyName(),
      staff: []
    }

    for (let userIndex = 0; userIndex < Math.floor(Math.random() * (4 - 1 + 1)) + 1; userIndex++) {
      provider.staff.push(data.users[userArrayCount]);
      userArrayCount++;
    }
    data.providers.push(provider);

  }

  let shuffleProviders = data.providers.slice(0);
  shuffle(shuffleProviders);

  for (let index = 0; index < data.jobs.length; index++) {
    data.jobs[index].provider = shuffleProviders[index];
  }

  return data
}
